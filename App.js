import React from 'react';
import { View, StatusBar} from "react-native"
import { Colors } from './src/config';
import Route from './src';
import { ToastComponent , Loader } from './src/components';
import { Home } from './src/containers';


const App = () => {

    return (
  
      <View style={{ flex: 1, backgroundColor: Colors.Primary }}>
        <StatusBar backgroundColor={Colors.Primary} barStyle="dark-content" />
        {/* <Loader /> */}
        <Route /> 
        <ToastComponent />
      </View>
    )
  
}

export default App;