import React from "react";
import { TouchableOpacity, TouchableOpacityProps } from "react-native"
import { Colors, Metrix } from "../../config";
import Icon from "react-native-vector-icons/FontAwesome5"
import styles from "./styles";


const FloatingButton: React.FC<TouchableOpacityProps | CustomProps>
    = ({ iconComp, iconName, buttonStyle, ...props }) => (
        <TouchableOpacity
            activeOpacity={Metrix.ActiveOpacity}
            {...props}
            style={{ ...styles.floatingButtonView, ...buttonStyle }}>
            {iconComp || <Icon name={iconName || "plus"} color={Colors.Text} size={18} />}
        </TouchableOpacity>
    )

interface CustomProps {
    iconComp: React.Component;
    iconName: String,
    buttonStyle: StyleSheet,
    onPress: Function
}


FloatingButton.defaultProps = {
    onPress: () => { },
    iconComp: undefined,
    iconName: "",
    buttonStyle: {}
}

export default React.memo(FloatingButton);
