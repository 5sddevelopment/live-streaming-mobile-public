import React from 'react';
import { Metrix, Colors } from '../../config';
import Toast, { BaseToast } from 'react-native-toast-message';

const ToastComponent = () => (
  <Toast
    config={{
      error: ({ text1, ...rest }) => (
        <BaseToast
          {...rest}
          style={{ borderLeftWidth: 20, borderLeftColor: Colors.Danger, height: Metrix.VerticalSize(80) }}
          text1Style={{
            fontSize: Metrix.customFontSize(15),
            fontWeight: '700',
            color: Colors.DarkGray
          }}
          text1=""
          text2Style={{
            fontSize: Metrix.customFontSize(14),
            fontWeight: '400',
            color: Colors.DarkGray
          }}
          text2NumberOfLines={2}
        />
      ),


      success: ({ text1, ...rest }) => (
        <BaseToast
          {...rest}
          style={{ borderLeftWidth: 20, borderLeftColor: Colors.Success, height: Metrix.VerticalSize(80) }}
          text1Style={{
            fontSize: Metrix.customFontSize(15),
            fontWeight: '700',
            color: Colors.DarkGray
          }}
          text1="Success"
          text2Style={{
            fontSize: Metrix.customFontSize(14),
            fontWeight: '400',
            color: Colors.DarkGray
          }}
          text2NumberOfLines={2}
        />
      )
    }}
    autoHide
    visibilityTime={4000}
  />
);


export default ToastComponent;
