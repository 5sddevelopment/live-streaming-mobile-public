import React from 'react';
import { BlurView } from '@react-native-community/blur';
import { Modal, View, Text, TouchableOpacity, TouchableHighlight, StyleSheet } from 'react-native'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { Metrix, Colors } from '../../config';
import ImageResizer from 'react-native-image-resizer';
import { PermissionsAndroid } from 'react-native';

export default ImagePickerModal = ({ visible, onRequestClose, onImageSelected }) => {

    const requestCameraPermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA);
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                return console.log("Camera permission given");
            } else {
                return console.log("Camera permission denied");
            }
        } catch (err) {
            throw console.warn(err);
        }
    };

    const openCamera = async () => {
        await requestCameraPermission()
        launchCamera({ mediaType: 'photo' }, (response) => {
            uploadImage(response);
            onRequestClose()
        });
    };

    const openGallery = () => {
        launchImageLibrary({ mediaType: 'photo' }, response => {
            uploadImage(response);
            onRequestClose()
        });
    };

    const uploadImage = response => {
        if (response.didCancel) {
        } else if (response.errorMessage) {
        } else{
              ImageResizer.createResizedImage(
                response?.assets[0].uri,
                720,
                480,
                'JPEG',
                100,
              )
                .then(compression => {
                  const itemImage = {
                    name:
                      compression.name ||
                      compression.uri.split('/')[compression.uri.split('/').length - 1],
                    type: response?.assets[0].type,
                    uri:
                      Platform.OS === 'android'
                        ? compression.uri
                        : compression.uri.replace('file://', ''),
                  };
                  onImageSelected(itemImage)
                })
                .catch(err => {
                  console.log('ERROR OCCURRED PICKER', err);
                });
        }
    }

    return (
        <Modal
            animationType="fade"
            transparent={true}
            visible={visible}
            onRequestClose={() => {
                onRequestClose()
            }}
        >
            <BlurView
                style={styles.absolute}
                blurType="dark"
                blurAmount={10}
                reducedTransparencyFallbackColor="black"
            />
            <TouchableOpacity
                activeOpacity={1}
                onPress={() => onRequestClose()}
                style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'flex-end',
                    paddingBottom: 20,
                }}>
                <TouchableHighlight>
                    <View
                        style={{
                            height: Metrix.VerticalSize(212),
                            width: Metrix.HorizontalSize(360),
                            borderRadius: Metrix.Radius,
                            alignItems: 'center',
                        }}>
                        <View
                            style={{
                                backgroundColor: 'white',
                                width: '100%',
                                borderRadius: Metrix.Radius,
                                marginBottom: Metrix.VerticalSize(5),
                            }}>
                            <View
                                style={{
                                    height: Metrix.VerticalSize(60),
                                    width: '100%', //Metrix.HorizontalSize(311),
                                    backgroundColor: 'white',
                                    borderRadius: Metrix.Radius,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    borderBottomWidth: 1,
                                    borderTopWidth: 1,
                                    borderColor: Colors.Shadow,
                                }}>
                                <Text
                                    style={{
                                        // fontFamily: Fonts["Poppins-Regular"],
                                        fontSize: Metrix.customFontSize(18),
                                        color: '#8f8f8f',
                                    }}>
                                    Select Picture
                                </Text>
                            </View>

                            <TouchableOpacity
                                onPress={() => {
                                    openCamera();
                                }}
                                style={{
                                    height: Metrix.VerticalSize(55),
                                    width: '100%', //Metrix.HorizontalSize(311),
                                    backgroundColor: 'white',
                                    borderRadius: Metrix.Radius,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    borderBottomWidth: 1,
                                    borderBottomColor: Colors.Shadow,
                                }}>
                                <Text
                                    style={{
                                        // fontFamily: Fonts["Poppins-Regular"],
                                        fontSize: Metrix.customFontSize(16),
                                        color: '#1d7af2',
                                        letterSpacing: 0.5,
                                    }}>
                                    Take Photo...
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    openGallery();
                                }}
                                style={{
                                    height: Metrix.VerticalSize(55),
                                    width: '100%', //Metrix.HorizontalSize(311),
                                    backgroundColor: 'white',
                                    borderRadius: Metrix.Radius,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    borderBottomWidth: 1,
                                    borderBottomColor: Colors.Shadow,
                                }}>
                                <Text
                                    style={{
                                        // fontFamily: Fonts["Poppins-Regular"],
                                        fontSize: Metrix.customFontSize(16),
                                        color: '#1d7af2',
                                        letterSpacing: 0.5,
                                    }}>
                                    Choose from library...
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity
                            activeOpacity={0.9}
                            onPress={() => {
                                onRequestClose()
                            }}
                            style={{
                                height: Metrix.VerticalSize(55),
                                width: '100%', //Metrix.HorizontalSize(311),
                                backgroundColor: 'white',
                                borderRadius: Metrix.Radius,
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}>
                            <Text
                                style={{
                                    // fontFamily: Fonts["Poppins-SemiBold"],
                                    fontSize: Metrix.customFontSize(16),
                                    fontWeight: 'bold',
                                    color: '#1d7af2',
                                    letterSpacing: 0.5,
                                }}>
                                Cancel
                            </Text>
                        </TouchableOpacity>
                    </View>
                </TouchableHighlight>
            </TouchableOpacity>
        </Modal>
    )
}

const styles = StyleSheet.create({
    absolute: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        height: '100%',
    },
})

