const Colors = {
    PlaceHolder: (opacity = "0.5") => `rgba(128, 128, 128, ${opacity})`,
    BlackOpacity: (opacity = "0.5") => `rgba(0, 0, 0, ${opacity})`,
    Transparent: "transparent",
    Primary: "#F4B23E",
    Secondary: "#F5F6F8",
    DarkGray: "#1E1E22",
    Black: "#000000",
    White: "#ffffff",
    Text: "#000000",
    Danger: "#FF494C",
    Success: "#22bb33",
}

export default Colors;