import {
    Home,
    Streaming
} from '../../containers'

export const HomeStack = [
    {
        name: 'Home',
        component: Home,
        key: 'Home',
    },
    {
        name: 'Streaming',
        component: Streaming,
        key: 'Streaming',
    },
]