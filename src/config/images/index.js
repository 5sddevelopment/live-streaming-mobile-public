const Images = {
    Logo: require("../../assets/images/logo.png"),
    Stream: require("../../assets/images/stream.png"),
    User: require("../../assets/images/user.png")
}

export default Images;