import { Platform } from "react-native"


//DEV URL
var baseUrl = "https://acktive.5stardesigners.net/aktive-poc/public/api/";
// var ip = "http://192.168.2.8"
// var port = "3001"
// var socketUrl = `${ip}:${port}`

var ip = "https://acktive.5stardesigners.net"
var port = "3008"
var socketUrl = `${ip}:${port}`
var peerConstraints = {
  iceServers: [
    {
      urls: 'stun:stun.5stardesigners.net:5742',
    },
    // {
    //   urls: 'turn:turn.5stardesigners.net:5742',
    //   username: '5sstunad',
    //   credential: '5tun68475s@ppd3'
    // },
  ],
}

var peerHost = {
  host: ip,
  secure: false,
  port,
  path: '/mypeer',
}

//UAT URL
//var baseUrl = "";

//LIVE URL
//var baseUrl = ";

/* All End Points should list here with Base URL */

const ENDPOINTS = {
  LOGIN: `${baseUrl}login`,
  SIGNUP: `${baseUrl}signup`,
  UPDATE_PROFILE: `${baseUrl}updateProfile`,
}


const appUrlAndroid = "";
const appUrlIOS = "";
const appUrlAndroidForReview = "";
const appUrlIOSForReview = "";
const appUrl = Platform.OS == "android" ? appUrlAndroid : appUrlIOS;
const appUrlForReview = Platform.OS == "android" ? appUrlAndroidForReview : appUrlIOSForReview;
const termsUrl = "";
const privacyUrl = "";
const contactSupportMail = "";
const instructionsUrl = "";


const constantVariables = {
  TempUser: { name: "David Smith", email: "david@yopmail.com", pass: "asd123$A", userId: 12 }
}

const configVariables = {
  baseUrl,
  socketUrl,
  peerConstraints,
  peerHost,
  appUrl,
  appUrlForReview,
  termsUrl,
  privacyUrl,
  contactSupportMail,
  instructionsUrl,
  ENDPOINTS,

}

export default {
  ...configVariables,
  ...constantVariables
}