import React, { useState, useEffect } from "react";
import { View, Text, TouchableOpacity, Image, AppState } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { Button, FastImage, Header, ImagePickerModal, Loader } from "../../components";
import { NavigationService, Images, Metrix, Variables, showToast, Colors } from "../../config";
import { AppAction } from "../../store/actions";
import styles from "./styles";
import { io } from 'socket.io-client';
import { mediaDevices } from 'react-native-webrtc';


const Home = ({ navigation, ...props }) => {
    const loader = useSelector((state) => state.AppReducer.loader)
    const socket = useSelector((state) => state.AppReducer.socket)
    const user = useSelector((state) => state.AppReducer.user)
    const [imagePicker, setImagePicker] = useState(false)
    const [isRequested, setIsRequested] = useState(false)
    const [isApproved, setIsApproved] = useState(false)
    const [btnLoader, setBtnLoader] = useState(false)
    const [host, setHost] = useState(null)
    const dispatch = useDispatch()

    useEffect(() => {
        //  navigation.addListener('focus', () => {
        //      console.log("navigation props", props.route)
        //     if (props.route.params?.resetState) {
        //         resetState()
        //     }
        // });
        dispatch(AppAction.LoaderTrue())
        connectSocket()
        return () => {
            // unsubscribe();
            socket?.disconnect()
            dispatch(AppAction.SetSocket(null))
        }
    }, [])

    const connectSocket = () => {
        const IO = io(Variables.socketUrl, {
            secure: true,
        });
        IO.on("connect", () => {
            console.log("socket connected " + IO.id)
            IO.emit("user_socket_connected", { user_id: user.userId, user_name: user.userName })
            dispatch(AppAction.SetSocket(IO))
        })
        IO.on("disconnect", (e) => {
            console.log("socket disconnected ")
            // IO.emit("user_disconnected")
            resetState()
        })
        IO.on("connect_error", (e) => {
            console.log("connection error ", e)
        });

        IO.on("get_host", ({ host }) => {
            console.log({ host })
            if (host?.[0]) {
                setHost(host[0])
                IO.emit("user_request")
            }
            else {
                showToast("error", "Stream not started")
                dispatch(AppAction.LoaderFalse())
            } 
        })
        IO.on("user_request", () => {
            dispatch(AppAction.LoaderFalse())
            setIsRequested(true)
        })
        IO.on("user_approve", () => {
            setIsRequested(false)
            setIsApproved(true)
        })
        IO.on("user_reject", () => {
            showToast("error", "Host rejected your request.")
            setIsRequested(false)
            setIsApproved(false)
        })
        IO.on("cancel_request", resetState)
        IO.on("host_disconnected", resetState)

    }

    const stream = () => {
        dispatch(AppAction.LoaderTrue())
        socket.emit("get_host")
    }

    const cancel = () => {
        setBtnLoader(true)
        socket.emit("cancel_request")
    }

    const resetState = () => {
        setBtnLoader(false)
        setIsRequested(false)
        setIsApproved(false)
        setHost(null)
    }


    const join = async () => {
        await mediaDevices.getUserMedia({
            audio: false,
            video: { frameRate: 30, facingMode: 'user' }
        });
        NavigationService.navigate("Streaming", { host, resetState })
    }

    const updateImage = (image) => {
        setImagePicker(false)

        let payload = { profile_image: image, full_name: user.userName }
        let formData = new FormData()
        for (i in payload) {
            formData.append(i, payload[i])
        }
        setTimeout(() => {
            dispatch(AppAction.UpdateProfile(formData))
        }, 500)
    }

    return (
        <View style={styles.container}>
            {loader && <Loader isModalLoader={true} />}
            <Header.Standard
                rightIconName={"log-out"}
                Heading={"Home"}
                onPressRight={() => dispatch(AppAction.Logout())}
            />
            <View style={styles.body}>
                <View style={styles.userInfo}>
                    <TouchableOpacity
                        activeOpacity={Metrix.ActiveOpacity}
                        onPress={() => setImagePicker(true)}>
                        <FastImage
                            localImage={""}
                            networkImage={user?.profile_image}
                            resizeMode={"cover"}
                            imageStyle={styles.photo}
                        />
                    </TouchableOpacity>
                    <Text style={styles.name} numberOfLines={1}>
                        <Text style={styles.hi}>Hi! </Text>
                        {user.displayName}
                    </Text>
                </View>
                <View style={styles.content}>
                    {!isRequested && !isApproved &&
                        <TouchableOpacity
                            activeOpacity={Metrix.ActiveOpacity}
                            style={styles.imageView}
                            onPress={stream}
                        >
                            <Image source={Images.Stream} style={styles.image} />
                            <Text style={styles.text}>Stream Me</Text>
                        </TouchableOpacity>
                    }
                    {isRequested &&
                        <>
                            <Text style={styles.text}>Your request is pending. Wait until admin approves it.</Text>
                            <Button.Standard
                                text="Cancel Request"
                                isLoading={btnLoader}
                                disabled={btnLoader}
                                onPress={cancel}
                                containerStyle={{ marginTop: Metrix.VerticalSize(35), backgroundColor: Colors.Danger }}
                                textStyle={{ color: Colors.White }}

                            />
                        </>
                    }
                    {isApproved &&
                        <>
                            <Text style={styles.text}>Admin has approved your request.</Text>
                            <Button.Standard
                                text="Join Waiting Room"
                                onPress={join}
                                containerStyle={{ marginTop: Metrix.VerticalSize(35), backgroundColor: Colors.Success }}
                                textStyle={{ color: Colors.White }}
                            />
                        </>
                    }
                </View>
            </View>
            <ImagePickerModal
                visible={imagePicker}
                onRequestClose={() => setImagePicker(false)}
                onImageSelected={updateImage}
            />
        </View>
    )

}



export default Home;