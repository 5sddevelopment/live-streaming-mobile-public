import { StyleSheet } from "react-native";
import { Colors, Fonts, Metrix } from "../../config";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.Secondary
    },
    body: {
        flex: 1,
        width: "100%",
        alignItems: "center",
        paddingHorizontal:Metrix.HorizontalSize(20)
    },
    userInfo: {
        alignItems: "center",
        marginVertical: Metrix.VerticalSize(20)
    },
    content: {
        flex: 1,
        alignItems: "center",
        paddingTop: Metrix.VerticalSize(100)
    },
    text: {
        textAlign:"center",
        fontFamily: Fonts["Montserrat-Bold"],
        fontSize: Metrix.FontLarge,
        color: Colors.Text
    },
    join:{
        textAlign:"center",
        fontFamily: Fonts["Montserrat-Medium"],
        fontSize: Metrix.FontMedium,
        color: Colors.Primary,
        marginTop:Metrix.VerticalSize(20), 
        textDecorationLine:"underline"
    },
    cancel:{
        textAlign:"center",
        fontFamily: Fonts["Montserrat-Medium"],
        fontSize: Metrix.FontMedium,
        color: Colors.Danger,
        marginTop:Metrix.VerticalSize(20), 
        textDecorationLine:"underline"
    },
    photo: {
        height: Metrix.VerticalSize(80),
        width: Metrix.VerticalSize(80),
        borderRadius: Metrix.VerticalSize(40)
    },
    imageView: {
        alignItems: "center",
    },
    image: {
        height: Metrix.VerticalSize(110),
        width: Metrix.VerticalSize(110),
        resizeMode: "contain",
        marginBottom: 10
    },
    name: {
        color: Colors.Black,
        fontFamily: Fonts["Montserrat-SemiBold"],
        fontSize: Metrix.FontLarge,
        padding: 10,
        textTransform: "capitalize"
    },
    hi: {
        fontFamily: Fonts["Montserrat-Regular"],
    }
})

export default styles