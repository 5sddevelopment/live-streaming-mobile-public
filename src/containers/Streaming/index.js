import React, { useState, useRef, useEffect } from "react";
import { View, Text, AppState } from "react-native";
import { Button, Header } from "../../components";
import styles from "./styles";
import { showToast, Variables } from "../../config";
import {
    RTCView,
    mediaDevices,
    registerGlobals
} from 'react-native-webrtc';
import Peer from 'react-native-peerjs';
import { useSelector } from "react-redux";
import KeepAwake from '@sayem314/react-native-keep-awake';

registerGlobals();

const Streaming = ({ navigation, ...props }) => {
    const socket = useSelector((state) => state.AppReducer.socket)
    const [mediaStream, setStream] = useState(null)
    const [status, setStatus] = useState("Joining")
    const [seconds, setSeconds] = useState(60)
    const peer = useRef()
    const interval = useRef(null);

    const userLive = () => setStatus("Live")
    const userJoined = () => setStatus("In Waiting room")
    const userRemove = () => {
        leave()
        showToast("error", "Host Removed you from live stream")
    }
    const hostDisconnect = () => {
        leave()
        showToast("error", "Stream ended by host")
    }
    const socketDisconnect = () => leave()


    useEffect(() => {
        const appState = AppState.addEventListener("change", nextAppState => {
            if (nextAppState == "background") {
                console.log("appstate", nextAppState)
                leave()
            }
        });
        socket.on("disconnect", socketDisconnect)
        socket.on("user_joined", userJoined)
        socket.on("user_live", userLive)
        socket.on("user_remove", userRemove)
        socket.on("host_disconnected", hostDisconnect)
        getMediaStream()

        return () => {
            if (interval.current) clearInterval(interval.current)

            peer?.current?.destroy();
            peer?.current?.disconnect();
            peer.current = null
            mediaStream?.getTracks().map(track => track.stop());

            socket.emit("user_disconnected")

            socket.off("disconnect", socketDisconnect)
            socket.off("user_joined", userJoined)
            socket.off("user_live", userLive)
            socket.off("user_remove", userRemove)
            socket.off("host_disconnected", hostDisconnect)

            appState.remove()
            console.log("unmounted")
        }
    }, [])

    useEffect(() => {
        if (status == "Live" && !interval.current) {
            interval.current = setInterval(() => {
                setSeconds(seconds => seconds - 1);
            }, 1000);
        }
        if (status == "Live" && seconds == 0) {
            leave()
        }
    }, [status, seconds])

    const getMediaStream = async () => {
        try {
            let stream = await mediaDevices.getUserMedia({
                audio: false,
                video: {
                    frameRate: 30,
                    facingMode: 'user'
                }
            });
            setStream(stream)
            addPeerConnection(stream)
        } catch (err) {
            // Handle Error
        };
    }

    const addPeerConnection = (stream) => {
        const { host } = props.route.params;
        peer.current = new Peer(Variables.peerConstraints);
        peer.current.on('error', console.log);
        peer.current.on('open', localPeerId => {
            
            socket.emit("user_connected", {
                peer_id: localPeerId
            })
            socket.on("user_connected",()=>{
                peer?.current?.call(host?.peer_id, stream)
            })

        });
    }

    const leave = () => {
        navigation?.navigate("Home")
        props.route.params?.resetState()
    }

    return (
        <View style={styles.container}>
            <KeepAwake />
            <Header.Standard Heading={`Live Stream`} />
            <View style={styles.body}>
                <View style={styles.rtcView}>
                    {mediaStream ?
                        <RTCView
                            style={{ flex: 1, width: "100%" }}
                            mirror={true}
                            objectFit={'cover'}
                            streamURL={mediaStream?.toURL()}
                            zOrder={0}
                        /> : null}
                    <View style={styles.topView}>
                        <View style={styles.roundView}><Text style={styles.text}>{status}</Text></View>
                        {status == "Live" ? <View style={styles.roundView}><Text style={styles.text}>{seconds}s</Text></View> : null}
                    </View>
                </View>
                <Button.FloatingButton
                    iconName={"stop"}
                    onPress={leave}
                />
            </View>
        </View>
    )

}



export default Streaming;