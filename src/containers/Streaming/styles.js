import { StyleSheet } from "react-native";
import { Colors, Fonts, Metrix } from "../../config";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.Secondary
    },
    body: {
        flex: 1,
        borderRadius: Metrix.Radius,
        justifyContent: "center",
        alignItems: "center",
    },
    rtcView: {
        flex: 1,
        width: "100%",
        position: "relative"
    },
    topView: {
        flexDirection: "row",
        justifyContent: "space-between",
        width: "100%",
        position: "absolute",
        top: Metrix.VerticalSize(10),
        left: 0,
        paddingHorizontal:Metrix.VerticalSize(10)
    },
    roundView: {
        paddingVertical: Metrix.VerticalSize(10),
        borderRadius: Metrix.VerticalSize(20),
        paddingHorizontal: Metrix.HorizontalSize(10),
        backgroundColor: Colors.BlackOpacity(0.5)
    },
    text: {
        color: Colors.White,
        fontFamily: Fonts["Montserrat-Medium"]
    }

})

export default styles