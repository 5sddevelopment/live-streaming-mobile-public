import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { connect } from "react-redux"
import { Button, Forminput, ImagePickerModal, FastImage } from "../../components";
import { Metrix, Images, Utils, NavigationService, showToast } from "../../config";
import { AppAction } from "../../store/actions";
import styles from "./styles";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

class SignUp extends Component {

    state = {
        image: "",
        display_name: "",
        full_name: "",
        email: "",
        password: "",
        consfirm_password: "",
        displayNameErr: "",
        nameErr: "",
        emailErrMsg: "",
        passErrMsg: "",
        confirmPassErrMsg: "",
        validEmail: true,
        imagePickerModalVisible: false
    }

    signup = () => {
        const { image, display_name, full_name, email, password, consfirm_password, validEmail } = this.state;
        const { Signup } = this.props;

        if (!display_name) this.setState({ displayNameErr: "Display name is a required field" })
        else if (!full_name) this.setState({ nameErr: "Full name is a required field" })
        else if (!email) this.setState({ emailErrMsg: "Email is a required field" })
        else if (!validEmail) this.setState({ emailErrMsg: "Please enter valid email address." })
        else if (!password) this.setState({ passErrMsg: "Password is a required field" })
        else if (!consfirm_password) this.setState({ confirmPassErrMsg: "Confirm Password is a required field" })
        else if (consfirm_password != password) this.setState({ confirmPassErrMsg: "Password does not match" })
        else if (!image) showToast("error", "Please upload your photo.")
        else {
            let payload = { profile_image: image, display_name, full_name, email, password }
            let formData = new FormData()
            for (i in payload) {
                formData.append(i, payload[i])
            }
            Signup(formData)
        }
    }

    validateEmail = (email) => {
        let validEmail = Utils.isEmailValid(email);
        this.setState({ email, validEmail, emailErrMsg: "" })
    }

    render() {
        const { image, display_name, displayNameErr, full_name, nameErr, email, password, consfirm_password, emailErrMsg, passErrMsg, confirmPassErrMsg, imagePickerModalVisible } = this.state
        return (
            <View style={styles.container}>
                <KeyboardAwareScrollView
                    contentInsetAdjustmentBehavior="automatic"
                    keyboardShouldPersistTaps="handled"
                    keyboardDismissMode="interactive"
                    showsVerticalScrollIndicator={false}
                    style={{ width: "100%" }}
                >
                    <View style={styles.content}>
                        <Text style={styles.headingStyle}>Signup</Text>
                        <TouchableOpacity activeOpacity={Metrix.ActiveOpacity} onPress={() => { this.setState({ imagePickerModalVisible: true }) }}>
                            <FastImage
                                localImage={image || Images.User}
                                networkImage={""}
                                resizeMode={"cover"}
                                imageStyle={styles.image}
                            />
                        </TouchableOpacity>
                        {!image && <Text style={styles.upload} >Upload Picture</Text>}
                        <Forminput.TextField
                            placeholder="Display Name"
                            returnKeyType="next"
                            autoCapitalize="none"
                            onChangeText={(display_name) => this.setState({ display_name, displayNameErr: "" })}
                            errMsg={displayNameErr}
                            value={display_name}
                            blurOnSubmit={false}
                            containerStyle={{ marginTop: Metrix.VerticalSize(25) }}
                            onSubmitEditing={() => { this.fullNameRef.focus() }}
                        />
                        <Forminput.TextField
                            placeholder="Full Name"
                            returnKeyType="next"
                            autoCapitalize="none"
                            onChangeText={(full_name) => this.setState({ full_name, nameErr: "" })}
                            errMsg={nameErr}
                            value={full_name}
                            blurOnSubmit={false}
                            containerStyle={{ marginTop: Metrix.VerticalSize(15) }}
                            reference={(ref) => { this.fullNameRef = ref }}
                            onSubmitEditing={() => { this.emailInputRef.focus() }}
                        />
                        <Forminput.TextField
                            placeholder="Email"
                            keyboardType="email-address"
                            returnKeyType="next"
                            autoCapitalize="none"
                            onChangeText={this.validateEmail}
                            errMsg={emailErrMsg}
                            value={email}
                            blurOnSubmit={false}
                            containerStyle={{ marginTop: Metrix.VerticalSize(15) }}
                            reference={(ref) => { this.emailInputRef = ref }}
                            onSubmitEditing={() => { this.passInputRef.focus() }}
                        />

                        <Forminput.TextField
                            placeholder="Password"
                            secureTextEntry
                            autoCapitalize="none"
                            returnKeyType="done"
                            onChangeText={(password) => this.setState({ password, passErrMsg: "" })}
                            errMsg={passErrMsg}
                            value={password}
                            containerStyle={{ marginTop: Metrix.VerticalSize(15) }}
                            reference={(ref) => { this.passInputRef = ref }}
                            onSubmitEditing={() => { this.confirmPassInputRef.focus() }}
                        />
                        <Forminput.TextField
                            placeholder="Confirm Password"
                            secureTextEntry
                            autoCapitalize="none"
                            returnKeyType="done"
                            onChangeText={(consfirm_password) => this.setState({ consfirm_password, confirmPassErrMsg: "" })}
                            errMsg={confirmPassErrMsg}
                            value={consfirm_password}
                            containerStyle={{ marginTop: Metrix.VerticalSize(15) }}
                            reference={(ref) => { this.confirmPassInputRef = ref }}
                            onSubmitEditing={this.signin}
                        />

                        <Button.Standard
                            text="Sign Up"
                            isLoading={this.props.loading}
                            disabled={this.props.loading}
                            onPress={this.signup}
                            containerStyle={{ marginTop: Metrix.VerticalSize(35) }}
                        />

                        <Text style={styles.signin} onPress={() => NavigationService.navigate("SignIn")}>Back to Sign In</Text>

                    </View>
                    <ImagePickerModal
                        visible={imagePickerModalVisible}
                        onRequestClose={() => this.setState({ imagePickerModalVisible: false })}
                        onImageSelected={(image) => { this.setState({ image }) }}
                    />
                </KeyboardAwareScrollView>
            </View>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return {
        Signup: (payload) => { dispatch(AppAction.SignUp(payload)) }
    }
}

function mapStateToProps(state) {
    return {
        loading: state.AppReducer.loader
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp)