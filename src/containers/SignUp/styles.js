import { StyleSheet } from "react-native";
import { Colors, Fonts, Metrix } from "../../config";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.Secondary
    },
    content: {
        flex: 1,
        paddingBottom: Metrix.VerticalSize(25),
        alignItems: "center",
        justifyContent: "center",
        paddingTop: Metrix.VerticalSize(80),
    },
    image: {
        height: Metrix.VerticalSize(120),
        width: Metrix.VerticalSize(120),
        borderRadius: Metrix.VerticalSize(60),
        marginBottom:Metrix.VerticalSize(5)
    },
    upload:{
        fontFamily:Fonts["Montserrat-Regular"],
        color:Colors.PlaceHolder()
    },
    headingStyle: {
        fontSize: Metrix.FontLarge,
        fontFamily: Fonts["Montserrat-Bold"],
        color: Colors.Text,
        marginBottom: Metrix.VerticalSize(30),
        letterSpacing: 0.77,
    },
    forgetPassText: {
        fontSize: Metrix.FontExtraSmall,
        fontFamily: Fonts["Montserrat-Regular"],
        color: Colors.Text,
        marginTop: Metrix.VerticalSize(25),
        textDecorationLine: "underline",
        letterSpacing: 0.42,
    },
    signin: {
        marginTop: Metrix.VerticalSize(20),
        fontFamily: Fonts["Montserrat-Medium"],
        color: Colors.Primary,
    }
})

export default styles