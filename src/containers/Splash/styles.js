import { StyleSheet } from "react-native";
import { Colors } from "../../config";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.Primary,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        width: "80%",
        height: "80%",
        resizeMode:"contain"
    }
})

export default styles