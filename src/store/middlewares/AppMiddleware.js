import { AppAction } from '../actions';
import Store from '..';
import { NavigationService, ApiCaller, Constants, showToast } from '../../config';
import { put } from 'redux-saga/effects';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class AppMiddleware {

    static *SignUp({ payload }) {
        const { navigate } = NavigationService
        try {
            let res = yield ApiCaller.Post(
                Constants.ENDPOINTS.SIGNUP,
                payload,
                {
                    "Content-Type": 'multipart/form-data',
                    "Accept": 'multipart/form-data'
                }
            )
            if (res.status == 200) {
                showToast("success", res.data.message)
                yield put(AppAction.SignUpSuccess())
                navigate("SignIn")
                console.log('%cSignIn Response', "color: green", ' => ', res)
            } else {
                showToast("error", res.data.message)
                yield put(AppAction.SignUpFailure())
                console.log('%cSignIn Response', "color: red", ' => ', res)
            }
        }
        catch (err) {
            yield put(AppAction.SignUpFailure())
            console.log(`%c${err.name}`, "color: red", ' => ', err)
        }
    }
    static *SignIn({ payload }) {
        const { replace } = NavigationService
        try {
            let res = yield ApiCaller.Post(
                Constants.ENDPOINTS.LOGIN,
                payload
            )
            if (res.status == 200) {
                yield put(AppAction.SignInSuccess(res.data.data))
                AsyncStorage.setItem("user", JSON.stringify(res.data.data))
                replace("Home")
                console.log('%cSignIn Response', "color: green", ' => ', res)
            } else {
                showToast("error", res.data.message)
                yield put(AppAction.SignInFailure())
                console.log('%cSignIn Response', "color: red", ' => ', res)
            }
        }
        catch (err) {
            yield put(AppAction.SignInFailure())
            console.log(`%c${err.name}`, "color: red", ' => ', err)
        }
    }

    static *SetSocket({ payload }) {
        try {
            yield put(AppAction.SetSocketSucess(payload))
        }
        catch (err) {
            yield put(AppAction.SetSocketFailure())
            console.log(`%c${err.name}`, "color: red", ' => ', err)
        }
    }

    static *UpdateProfile({ payload }) {
        try {
            const { user } = yield Store.getState().AppReducer
            let res = yield ApiCaller.Post(
                `${Constants.ENDPOINTS.UPDATE_PROFILE}/${user.userId}`,
                payload,
                {
                    "Authorization": `Bearer ${user.token}`,
                    "Content-Type": 'multipart/form-data',
                    "Accept": 'multipart/form-data'
                }
            )
            if (res.status == 200) {
                showToast("success", res.data.message)
                let userData = { ...user, ...res.data.data }
                yield put(AppAction.UpdateProfileSuccess(userData))
                AsyncStorage.setItem("user", JSON.stringify(userData))
                console.log('%Profile Response', "color: green", ' => ', res)
            } else {
                showToast("error", res.data.message)
                yield put(AppAction.UpdateProfileFailure())
                console.log('%Profile Response', "color: red", ' => ', res)
            }
        }
        catch (err) {
            yield put(AppAction.UpdateProfileFailure())
            console.log(`%c${err.name}`, "color: red", ' => ', err)
        }
    }

    static * Logout() {
        const { reset_0 } = NavigationService
        try {
            yield AsyncStorage.removeItem("user")
            yield put(AppAction.LogoutSuccess())
            reset_0('SignIn')
        }
        catch (err) {
            yield put(AppAction.LogoutFailure())
            console.log(`%c${err.name}`, "color: red", ' => ', err)
        }
    }
}