import {
    SIGNUP, SIGNUP_SUCCESS, SIGNUP_FAILURE,
    SIGNIN, SIGNIN_SUCCESS, SIGNIN_FAILURE,
    LOGOUT, LOGOUT_SUCCESS, LOGOUT_FAILURE,
    LOADER_TRUE, LOADER_FALSE, SET_SOCKET, SET_SOCKET_SUCCESS, SET_SOCKET_FAILURE, UPDATE_PROFILE, UPDATE_PROFILE_SUCCESS, UPDATE_PROFILE_FAILURE,
} from '../constants';

const initialState = {
    user: {},
    loader: false,
    socket: null
}

export default function AppReducer(state = initialState, action) {
    switch (action.type) {

        case SIGNUP:
            state = {
                ...state,
                loader: true
            }
            break;
        case SIGNUP_SUCCESS:
            state = {
                ...state,
                loader: false
            }
            break;
        case SIGNUP_FAILURE:
            state = {
                ...state,
                loader: false
            }
            break;
        case SIGNIN:
            state = {
                ...state,
                loader: true
            }
            break;
        case SIGNIN_SUCCESS:
            state = {
                ...state,
                user: action.payload,
                loader: false
            }
            break;
        case SIGNIN_FAILURE:
            state = {
                ...state,
                loader: false
            }
            break;

        case SET_SOCKET_SUCCESS:
            state = {
                ...state,
                socket: action.payload,
                loader: false
            }
            break;
        case SET_SOCKET_FAILURE:
            state = {
                ...state,
                loader: false
            }
            break;

        case UPDATE_PROFILE:
            state = {
                ...state,
                loader: true
            }
            break;
        case UPDATE_PROFILE_SUCCESS:
            state = {
                ...state,
                user: action.payload,
                loader: false
            }
            break;
        case UPDATE_PROFILE_FAILURE:
            state = {
                ...state,
                loader: false
            }
            break;

        case LOGOUT:
            state = {
                ...state,
                loader: true
            }
            break;
        case LOGOUT_SUCCESS:
            state = {
                user: {},
                loader: false
            }
            break;
        case LOGOUT_FAILURE:
            state = {
                ...state,
                loader: false
            }
            break;

        case LOADER_TRUE:
            state = {
                ...state,
                loader: true
            }
            break;

        case LOADER_FALSE:
            state = {
                ...state,
                loader: false
            }
            break;

        default:
            break;
    }

    return state;
}