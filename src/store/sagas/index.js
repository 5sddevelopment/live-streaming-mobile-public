import { AppMiddleware } from "../middlewares"
import { takeLatest, all } from 'redux-saga/effects'
import { LOGOUT, SIGNIN, SIGNUP, SET_SOCKET, UPDATE_PROFILE } from "../constants"

export function* Sagas() {
    yield all([
        yield takeLatest(SIGNUP, AppMiddleware.SignUp),
        yield takeLatest(SIGNIN, AppMiddleware.SignIn),
        yield takeLatest(SET_SOCKET, AppMiddleware.SetSocket),
        yield takeLatest(UPDATE_PROFILE, AppMiddleware.UpdateProfile),
        yield takeLatest(LOGOUT, AppMiddleware.Logout)
    ])
}