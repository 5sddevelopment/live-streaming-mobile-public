import {
    SIGNUP, SIGNUP_SUCCESS, SIGNUP_FAILURE,
    SIGNIN, SIGNIN_SUCCESS, SIGNIN_FAILURE,
    SET_SOCKET, SET_SOCKET_SUCCESS, SET_SOCKET_FAILURE,
    UPDATE_PROFILE, UPDATE_PROFILE_SUCCESS, UPDATE_PROFILE_FAILURE,
    LOGOUT, LOGOUT_SUCCESS, LOGOUT_FAILURE,
    LOADER_FALSE, LOADER_TRUE
} from '../constants'


export default class AppAction {
    static SignUp(payload) {
        return {
            type: SIGNUP,
            payload
        }
    }
    static SignUpSuccess() {
        return {
            type: SIGNUP_SUCCESS,
        }
    }
    static SignUpFailure() {
        return {
            type: SIGNUP_FAILURE
        }
    }

    static SignIn(payload) {
        return {
            type: SIGNIN,
            payload
        }
    }
    static SignInSuccess(payload) {
        return {
            type: SIGNIN_SUCCESS,
            payload
        }
    }
    static SignInFailure() {
        return {
            type: SIGNIN_FAILURE
        }
    }

    static SetSocket(payload) {
        return {
            type: SET_SOCKET,
            payload
        }
    }
    static SetSocketSucess(payload) {
        return {
            type: SET_SOCKET_SUCCESS,
            payload
        }
    }
    static SetSocketFailure() {
        return {
            type: SET_SOCKET_FAILURE
        }
    }

    static UpdateProfile(payload) {
        return {
            type: UPDATE_PROFILE,
            payload,
        }
    }
    static UpdateProfileSuccess(payload) {
        return {
            type: UPDATE_PROFILE_SUCCESS,
            payload
        }
    }
    static UpdateProfileFailure() {
        return {
            type: UPDATE_PROFILE_FAILURE
        }
    }

    static Logout() {
        return {
            type: LOGOUT
        }
    }
    static LogoutSuccess() {
        return {
            type: LOGOUT_SUCCESS
        }
    }
    static LogoutFailure() {
        return {
            type: LOGOUT_FAILURE
        }
    }

    static LoaderTrue() {
        return {
            type: LOADER_TRUE
        }
    }
    static LoaderFalse() {
        return {
            type: LOADER_FALSE
        }
    }

}